# COVID-19 Tracker

### [Live Site](https://covid19statswebsite.netlify.com/)

![COVID-19 Tracker](https://i.ibb.co/X87BqVY/Screenshot-2020-04-13-at-10-14-58.png)

## Stay up to date with new projects

## Introduction

This ia a full COVID-19 Tracker. In this project React, Charts.JS and Material UI has been used.

API used: https://covid19.mathdro.id/api

Setup:

- run `npm i && npm start`
